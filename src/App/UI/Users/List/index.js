/* @flow */

import * as React from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'react-redux';
import UsersListView from './UsersListView';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import compose from 'recompose/compose';

import fetchUsers from '../../../Application/fetchUsers';
import FetchUsers from '../../../Application/Query/FetchUsers';
import { withWidth } from '@material-ui/core';

type Props = {|
    dispatch: Dispatch,
        users: ?UsersListView,
|};

const styles = theme => ({
    root: {
        padding: "20px",
        maxWidth: "750px",
        margin: "0 auto"
    },
    card: {
        padding: "10px",
        margin: "0 0 10px"
    },
    userImg: {
        [theme.breakpoints.between('xs', 'sm')]: {
            borderRadius: "100%"
        }
    },
    pagination: {
        padding: "20px"
    },
    pageCounter: {
        textAlign: "center"
    },
    firstName: {
        textTransform: "capitalize",
        padding: "5px 0"
    },
    email: {
        [theme.breakpoints.up('sm')]: {
            padding: "30px 0 0"
        }
    },
    lastName: {
        textTransform: "uppercase",
        padding: "5px 0"
    },
    button: {
        minWidth: "120px",
    },
    buttonUser: {
        margin: "15px 0",
        [theme.breakpoints.down('sm')]: {
            margin: "15px 10px 0 0"
        }
    }

});

export class UsersList extends React.Component<Props> {
    constructor(props) {
        super(props);

        this.state = {
            page: 1,
            rowsPerPage: 5,
            filteredUsers: null
        }
    }

    componentDidMount() {
        this.displayUsers(this.state.page);
    }

    nextPage(currentPage) {
        if (currentPage < this.props.users.getTotalPages()) {
            const newPage = currentPage + 1;

            this.setState({
                page: newPage
            });

            this.displayUsers(newPage);
        }
    }

    previousPage(currentPage) {
        if (currentPage > 1) {
            const newPage = currentPage - 1;

            this.setState({
                page: newPage
            });

            this.displayUsers(newPage);
        }
    }

    displayUsers = (page) => {
        this.props.dispatch(fetchUsers(new FetchUsers(page, this.state.rowsPerPage)));
    }

    render() {
        let { users, classes, className } = this.props;
        let { page } = this.state;

        return (
            <div className={classNames(classes.root, className)}>

                {users && users.getUsers(page).map(user => {
                    const fullName = user.firstname + ' ' + user.lastname;
                    return (
                        <Paper key={user.id} className={classNames(classes.card, 'list-wrap')}>
                            <Grid container spacing={16}>
                                <Grid item>
                                    <img className={classNames(classes.userImg)} src={user.picture} title={fullName} alt={fullName} />
                                </Grid>
                                <Grid item xs container>
                                    <Grid item sm>
                                        <div className={classNames(classes.firstName)}>{user.lastname}</div>
                                        <div className={classNames(classes.lastName)}>{user.firstname}</div>
                                        <div className={classNames(classes.email)}>{user.email}</div>
                                    </Grid>
                                    <Grid item xs={12} sm={3} container>
                                        <Grid item sm={12}>
                                            <Button size="small" variant="contained" color="primary" className={classNames(classes.button, classes.buttonUser)}>Edit</Button>
                                        </Grid>
                                        <Grid item sm={12}>
                                            <Button size="small" variant="contained" color="secondary" className={classNames(classes.button, classes.buttonUser)}>Delete</Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Paper>
                    );
                })}
                {users && users.getTotalPages() > 1 &&
                    <div id="pagination" className={classNames(classes.pagination)}>
                        <Grid container justify="center" alignItems="center">
                            <Grid item>
                                <Button
                                    className={classNames(classes.button, 'btn-prev')}
                                    disabled={users ? !users.hasPreviousPage(page) : false}
                                    onClick={this.previousPage.bind(this, page)}
                                    variant="contained"
                                    color="primary"
                                >
                                    Previous
                            </Button>
                            </Grid>
                            <Grid item xs>
                                <div className={classNames(classes.pageCounter)}>
                                    Page {page} / {users ? users.getTotalPages() : ''}
                                </div>
                            </Grid>
                            <Grid item>
                                <Button
                                    className={classNames(classes.button, 'btn-next')}
                                    disabled={users ? !users.hasNextPage(page) : false}
                                    onClick={this.nextPage.bind(this, page)}
                                    variant="contained"
                                    color="primary"
                                >
                                    Next
                            </Button>
                            </Grid>
                        </Grid>
                    </div>
                }
            </div>
        );
    }
}

const mapStateToProps = ({ usersListReducers }) => {
    return { ...usersListReducers };
};

export default compose(
    withWidth(),
    withStyles(styles),
    connect(mapStateToProps)
)(UsersList);
