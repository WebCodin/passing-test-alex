import React from 'react';
import { shallow } from 'enzyme';

import { UsersList } from './index';

const list = [
    {
        id: 'id-1',
        email: 'user1@example.com',
        firstname: 'firstname-1',
        lastname: 'lastname-1',
        picture: 'picture-1'
    },
    {
        id: 'id-2',
        email: 'user1@example.com',
        firstname: 'firstname-2',
        lastname: 'lastname-2',
        picture: 'picture-2'
    }
];

describe('UsersList', () => {

    it('User list is load', () => {
        const users = {
            getUsers: () => list,
            hasPreviousPage: () => false,
            hasNextPage: () => true,
            getTotalPages: () =>  4
        }
    
        const wrapper = shallow(<UsersList users={users} dispatch={() => { }} classes={{}} />);
        expect(wrapper.find(".list-wrap")).not.toHaveLength(0);
    })

    it('User list is NOT load', () => {
        const users = {
            getUsers: () => [],
            hasPreviousPage: () => false,
            hasNextPage: () => true,
            getTotalPages: () =>  4
        }

        const wrapper = shallow(<UsersList users={users} dispatch={ () => {} } classes={{}} />);
        expect(wrapper.find(".list-wrap")).toHaveLength(0);
    })

    it('User list NOT has previous page', () => {
        const users = {
            getUsers: () => list,
            hasPreviousPage: () => false,
            hasNextPage: () => true,
            getTotalPages: () =>  4
        }

        const wrapper = shallow(<UsersList users={users} dispatch={ () => {} } classes={{}} />);
        expect(wrapper.find(".btn-prev").prop('disabled')).toBe(true);
    })

    it('User list NOT has next page', () => {
        const users = {
            getUsers: () => list,
            hasPreviousPage: () => false,
            hasNextPage: () => false,
            getTotalPages: () =>  4
        }

        const wrapper = shallow(<UsersList users={users} dispatch={ () => {} } classes={{}} />);
        expect(wrapper.find(".btn-next").prop('disabled')).toBe(true);
    })

    it('User list NOT has paginations when pages less than two', () => {
        const users = {
            getUsers: () => list,
            hasPreviousPage: () => false,
            hasNextPage: () => false,
            getTotalPages: () =>  1
        }

        const wrapper = shallow(<UsersList users={users} dispatch={ () => {} } classes={{}} />);
        expect(wrapper.find("#pagination")).toHaveLength(0);
    })

})