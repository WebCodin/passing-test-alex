import React from 'react';
import { mount } from 'enzyme';

import { AutoComplete } from './AutoComplete';

describe('UsersSearch', () => { 
    it('AutoComplete return value', () => {
        const onSearchMock = jest.fn();
        const wrapper = mount(<AutoComplete suggestions={[]} classes={{}} handleFilter={onSearchMock} />);
        wrapper.find('input').simulate('change', { target: { value: 'firstname' } })
        expect(onSearchMock).toBeCalledWith('firstname');
    });
});