/* @flow */

import * as React from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'react-redux';
import UserView from './UserView';
import Search from '@material-ui/icons/Search';
import styled from 'styled-components'
import filterUsers from '../../../Application/filterUsers';
import FilterUsers from '../../../Application/Query/FilterUsers';

import AutoComplete from './AutoComplete';

const Wrapper = styled.div`
    padding: 59px 0;
    border: 1px solid #0069c0;
`;

const InnerWrapper = styled.div`
    display: flex;
    align-items: center;
    max-width: 750px;
    margin: 0 auto;
`;

const IconWrap = styled.div`
    color: #0069c0;
    padding: 5px 10px 0 0;
`;

type Props = {|
    dispatch: Dispatch,
    users: ?Array<UserView>,
|};

export class UsersSearch extends React.Component<Props> {
    constructor(props) {
        super(props);

        this.state = {
            allUsers: []
        }
    }
    
    componentDidMount() {
        this.setState({
            allUsers: filterUsers(new FilterUsers('', undefined)).payload.map(
                ({ id, email, name }) => new UserView(id.value, email, name.first, name.last))
        })
    }

    handleFilter = (value) => {
        this.props.dispatch(filterUsers(new FilterUsers(value, undefined)))
    }

    render() {
        let { allUsers } = this.state;

        return (
            <Wrapper>
                {allUsers && 
                    <InnerWrapper>
                        <IconWrap>
                            <Search />
                        </IconWrap>
                        <AutoComplete suggestions={allUsers} handleFilter={this.handleFilter} />
                    </InnerWrapper>
                }
            </Wrapper>
        );
    }
}

const mapStateToProps = ({ usersSearchReducers }) => {
    return { ...usersSearchReducers };
};

export default connect(mapStateToProps)(UsersSearch);
